package Spring;


import org.springframework.context.support.GenericXmlApplicationContext;

import dao.PanierDao;
import entity.Panier;



public class AppSpring {

	public static void main(String[] args) {
		GenericXmlApplicationContext appCtx = new GenericXmlApplicationContext("classpath:META-INF/projet-context.xml");
		try {

			PanierDao panierDao = appCtx.getBean(PanierDao.class);
			Panier panier = new Panier();
			panierDao.createPanier(panier);
			
		} finally {
			appCtx.close();
		}


	}

}
