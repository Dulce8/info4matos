package entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
@Table(name="article")
@SequenceGenerator(name = "article_seq", allocationSize = 1)
public class Article {
		@Id
		@Column(name="id")
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="article_seq")
		private Long id;
		
		private String designation;
		
		@Column(name="image")
		private String cheminFichierImage;
		
		private double prix;
		
		private long stock;
		

		@Column(name="content")
		private String description;
		
		//@JsonFormat(shape=Shape.STRING, pattern="dd-MM-yyyy") //pour le format de la date
		@Temporal(TemporalType.DATE)
		@Column(name="dateMiseEnLigne")
		@JsonFormat(shape=Shape.STRING) //prendre le format par default
		private Date miseEnLigne;
		
		@ManyToOne
		@JoinColumn(name="categorie_id")
		private Categorie categorie;
		
		@ManyToOne
		@JoinColumn(name="constructeur_id")
		private Constructeur constructeur; 
		

		
		
		public Article() {
			super();
		}


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getDesignation() {
			return designation;
		}


		public void setDesignation(String designation) {
			this.designation = designation;
		}


		public String getCheminFichierImage() {
			return cheminFichierImage;
		}


		public void setCheminFichierImage(String cheminFichierImage) {
			this.cheminFichierImage = cheminFichierImage;
		}


		public double getPrix() {
			return prix;
		}


		public void setPrix(double prix) {
			this.prix = prix;
		}


		public String getDescription() {
			return description;
		}


		public void setDescription(String description) {
			this.description = description;
		}


		public Date getMiseEnLigne() {
			return miseEnLigne;
		}


		public void setMiseEnLigne(Date miseEnLigne) {
			this.miseEnLigne = miseEnLigne;
		}


		public Categorie getCategorie() {
			return categorie;
		}


		public void setCategorie(Categorie categorie) {
			this.categorie = categorie;
		}


		public Constructeur getConstructeur() {
			return constructeur;
		}


		public void setConstructeur(Constructeur constructeur) {
			this.constructeur = constructeur;
		}
		
		public long getStock() {
			return stock;
		}


		public void setStock(long stock) {
			this.stock = stock;
		}

		
}
