package entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="constructeur")
@SequenceGenerator(name = "constructeur_seq", allocationSize = 1)
public class Constructeur {
	
	  @Id
	  @Column(name="id")
	  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="constructeur_seq")
	  private Long id;

	  @Basic
	  @Column(name="nom")
	  private String nom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	  
	  

}
