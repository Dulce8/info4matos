package entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="panier")
@SequenceGenerator(name = "panier_seq", allocationSize = 1)
public class Panier {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="panier_seq")
	  private Long id;
	
	//pour recuperer tous les id des articles
	@ManyToMany(fetch=FetchType.EAGER )
	@JoinTable(name = "panierarticle",
    	joinColumns = @JoinColumn(name = "panier_id"),
    	inverseJoinColumns = @JoinColumn(name = "article_id"))
	  private List<Article> articles = new ArrayList<>();

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
	
}
