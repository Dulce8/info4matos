package entity;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="categorie")
@SequenceGenerator(name = "categorie_seq", allocationSize = 1)
public class Categorie {
	 @Id
	  @Column(name="id")
	  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="categorie_seq")
	  private Long id;

	  @Basic
	  @Column(name="nom")
	  private String nom;
	  
	  
	  //jsonignore permet de pas tenir en compte la liste d'articles pour mon affiche de categories
	  @JsonIgnore
	  @OneToMany(mappedBy="categorie")
	  private List<Article> articles;
	  
	

	public List<Article> getArticles() {
		return articles;
	}


	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}


	public Categorie() {
		super();
	}

	
	public Categorie( String nom) {
		super();
		this.nom = nom;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	  
}
