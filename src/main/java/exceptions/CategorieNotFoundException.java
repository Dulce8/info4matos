package exceptions;

public class CategorieNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public  CategorieNotFoundException() {
		super("categorie n'existe pas");
	}
}
