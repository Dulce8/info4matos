package exceptions;

public class PanierNotFoundException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PanierNotFoundException() {
		super("panier n'existe pas ");
	}
}
