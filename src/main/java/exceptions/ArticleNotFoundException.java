package exceptions;

public class ArticleNotFoundException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArticleNotFoundException() {
		super("Article n'existe pas");
	}
}
