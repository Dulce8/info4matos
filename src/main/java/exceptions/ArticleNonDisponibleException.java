package exceptions;

public class ArticleNonDisponibleException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArticleNonDisponibleException() {
		super("Article non disponible");
	}
}
