package dao;


import entity.Panier;

public interface PanierDao {
	public void createPanier(Panier panier);
	public Panier get (long id);
	public void deletePanier(long id);
	
}
