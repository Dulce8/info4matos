package dao;

import java.util.List;

import entity.Article;

public interface ArticleDao {
	public Article getArticleById(long id);
	public List<Article> getAllByNomCategorie(String nomCategorie);
	public void createArticle (Article art);
	public void deleteArticleById(long id);
	public List<Article> getAllArticles();
	
}
