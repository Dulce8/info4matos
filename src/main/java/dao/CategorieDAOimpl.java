package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Categorie;

@Repository 
public class CategorieDAOimpl implements CategorieDAO {
	

	@PersistenceContext
	private EntityManager entityManager;
	
	public CategorieDAOimpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public CategorieDAOimpl() {}
	
	
	@Override
	@Transactional
	public void createCategorie(Categorie cat) {
			entityManager.persist(cat);
	}

	@Override
	public Categorie getCategorieById(long id) {
			return entityManager.find(Categorie.class, id);
	}

	@Override
	@Transactional
	public void deleteCategoirie(Categorie cat) {
		entityManager.remove(cat);
	}

	@Override
	public List<Categorie> getAllCategorie() {
		return entityManager.createQuery("select c from Categorie c order by c.nom", Categorie.class).getResultList();
	}

	@Override
	public Categorie getCategorieByNom(String nom) {
		
		List<Categorie> categories = entityManager.createQuery("select c from Categorie c where c.nom = :nom", Categorie.class)
				.setParameter("nom", nom)
				.setMaxResults(1) //me donne la premiere ligne qu'il trouve
				.getResultList();
		return categories.isEmpty() ? null : categories.get(0);
	}


	@Override
	@Transactional
	public void deleteCategorieById(long id) {
		entityManager.createQuery("delete from Categorie c where c.id = :id")
        .setParameter("id", id)
        .executeUpdate();
	}
	
//	public Categorie getAvecArticles(long id) {
//		return entityManager.createQuery("select c from Categorie c left join fetch c.articles cher c.id = :id", )
//	}

}
