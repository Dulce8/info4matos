package dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Panier;

//@repository et @component est la meme chose
@Repository 
public class PanierDaoImpl implements PanierDao {

	//pour faire de l'injection d'un entity manager 
	@PersistenceContext
	private EntityManager entityManager;
	
	public PanierDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public PanierDaoImpl() {}
	
	// es para poder utiliser le rollback
//	@Override
//	@Transactional(rollbackFor=TaillePanierException.class)
//	public void createPanier(Panier panier) throws TaillePanierException{
//		if(panier.getArticles().size()>=10) {
//			throw new TaillePanierException;
//		}
//		entityManager.persist(panier);
//	}
	
	
	@Override
	@Transactional
	public void createPanier(Panier panier) {
		entityManager.persist(panier);
	}
	


	@Override
	public Panier get(long id) {
		
		return entityManager.find(Panier.class, id);
	}

	@Override
	@Transactional
	public void deletePanier(long id) {
		entityManager.createQuery("delete from Panier p where p.id = :id")
		.setParameter("id", id)
		.executeUpdate();
		
	}
	
	
	
	

}
