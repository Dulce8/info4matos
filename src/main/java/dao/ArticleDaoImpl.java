package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Article;

@Repository 
public class ArticleDaoImpl implements ArticleDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public ArticleDaoImpl() {}
	public ArticleDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public Article getArticleById(long id) {
		return entityManager.find(Article.class, id);
	}

	@Override
	public List<Article> getAllByNomCategorie(String nomCategorie) {
		List<Article> categories = entityManager.createQuery("select a from Article a where a.categorie.nom = :nom", Article.class)
												.setParameter("nom", nomCategorie)
												.getResultList();
		return categories;
	}
	@Override
	@Transactional
	public void createArticle(Article art) {
		entityManager.persist(art);
		
	}
	
	@Override
	@Transactional
	public void deleteArticleById(long id) {
		entityManager.createQuery("delete from Article a where a.id = :id")
		.setParameter("id", id)
		.executeUpdate();
		
	}
	@Override
	public List<Article> getAllArticles() {
		return entityManager.createQuery("select c from Article c ", Article.class)
				.getResultList();
	}
	
}
