package dao;

import java.util.List;

import entity.Constructeur;

public interface ConstructeurDao {
	public void createConstructeur(Constructeur c);
	public Constructeur getConstructeurById(long id);
	public void deleteConstructeur (Constructeur c);
	public List<Constructeur> getAllConstructeur();
	public Constructeur getConstructeurByNom(String nom);
}
