package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entity.Categorie;

public class ModifierCategorieApp {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("catalogue");
		EntityManager entityManager = emf.createEntityManager();
		try {
			Categorie categorie = entityManager.find(Categorie.class, 7L);
			entityManager.getTransaction().begin();
			categorie.setNom("BigMegaSpecial");
			entityManager.getTransaction().commit();
			
		} finally {
			entityManager.close();
			emf.close();
		}
	}
	
	
	
}
