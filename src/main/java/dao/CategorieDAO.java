package dao;

import java.util.List;

import entity.Categorie;

public interface CategorieDAO {
	public void createCategorie(Categorie cat);
	public Categorie getCategorieById(long id);
	public void deleteCategoirie(Categorie cat);
	public List<Categorie> getAllCategorie();
	public Categorie getCategorieByNom(String nom);
	public void deleteCategorieById(long id);
	
}
