package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Constructeur;

@Repository 
public class ConstructeurDaoImpl implements ConstructeurDao {
	@PersistenceContext
	private EntityManager entityManager;
	
	public ConstructeurDaoImpl() {}
	public ConstructeurDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	
	@Override
	@Transactional
	public void createConstructeur(Constructeur c) {
		entityManager.persist(c);
	}

	@Override
	public Constructeur getConstructeurById(long id) {
			return entityManager.find(Constructeur.class, id);
		
	}

	@Override
	public void deleteConstructeur(Constructeur c) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Constructeur> getAllConstructeur() {
	
		return entityManager.createQuery("select i from Constructeur i", Constructeur.class)
				.getResultList();
	}

	@Override
	public Constructeur getConstructeurByNom(String nom) {
		// TODO Auto-generated method stub
		return null;
	}

}
