package service;

import java.util.List;

import entity.Panier;
import exceptions.ArticleNonDisponibleException;
import exceptions.PanierNotFoundException;

public interface PanierService {

	public void update (Panier panier);
	public void create (Panier panier);
	public List<Panier> getAllPaniers();
	public Panier getPanierById(long id) throws PanierNotFoundException;
	public Panier validatePanier(long id) throws PanierNotFoundException, ArticleNonDisponibleException;
	public void delete(long id) throws PanierNotFoundException;
}
