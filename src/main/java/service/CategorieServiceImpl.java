package service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CategorieDAO;
import entity.Categorie;
import exceptions.CategorieNotFoundException;

@Service
public class CategorieServiceImpl implements CategorieService{

	private CategorieDAO categorieDao;
	
	public CategorieServiceImpl(CategorieDAO categorieDao) {
		this.categorieDao = categorieDao;
	}


	//ici on fait l'update
	@Override
	@Transactional(rollbackFor = CategorieNotFoundException.class)//pour pas commiter s'il y a l'exception
	public void update(Categorie categorie) throws CategorieNotFoundException{
		Categorie categorieExistante = categorieDao.getCategorieById(categorie.getId());	
		if(categorieExistante == null) {
			throw new CategorieNotFoundException();
		}
		categorieExistante.setNom(categorie.getNom());
	}


	@Override
	public Categorie getCategorieById(long idCategorie) throws CategorieNotFoundException{
			
		Categorie cat = categorieDao.getCategorieById(idCategorie);
			
			if(cat == null) {
				throw new CategorieNotFoundException();
			}
				return cat;
	}


	@Override
	public List<Categorie> getAllCategories() {
		return categorieDao.getAllCategorie();
	}


	@Override
	public void deleteCategorie(long idCategorie) throws CategorieNotFoundException{
		getCategorieById(idCategorie);//aqui hace la verification du id et fait l'exception s'il le faut
		categorieDao.deleteCategorieById(idCategorie);
		
	}


	@Override
	public void create(Categorie newCat) {
		categorieDao.createCategorie(newCat);
		
	}

}
