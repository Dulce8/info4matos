package service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ArticleDao;
import entity.Article;
import exceptions.ArticleNotFoundException;

@Service
public class ArticleServiceImpl implements ArticleService {

	private ArticleDao articleDao;
	
	public ArticleServiceImpl(ArticleDao articleDao) {
		this.articleDao = articleDao;
	}

	@Override
	@Transactional(rollbackFor = ArticleNotFoundException.class)
	public void update(Article article) throws ArticleNotFoundException {
		Article articleExistante = articleDao.getArticleById(article.getId());
		if(articleExistante == null) {
			throw new ArticleNotFoundException();
		}
		articleExistante.setCategorie(article.getCategorie());
		articleExistante.setCheminFichierImage(article.getCheminFichierImage());
		articleExistante.setConstructeur(article.getConstructeur());
		articleExistante.setDescription(article.getDescription());
		articleExistante.setDesignation(article.getDesignation());
		articleExistante.setMiseEnLigne(article.getMiseEnLigne());
		articleExistante.setPrix(article.getPrix());
		articleExistante.setStock(article.getStock());

	}

	@Override
	public Article getArticleById(long idArticle) throws ArticleNotFoundException {
		Article art = articleDao.getArticleById(idArticle);
		if(art == null) {
			throw new ArticleNotFoundException();
		}
		return art;
	}

	@Override
	public List<Article> getAllArticles() {
		return articleDao.getAllArticles();
	}

	@Override
	public void deleteArticle(long idArticle) throws ArticleNotFoundException {
		getArticleById(idArticle);
		articleDao.deleteArticleById(idArticle);

	}

	@Override
	public void create(Article article) {
		articleDao.createArticle(article);

	}

}
