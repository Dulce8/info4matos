package service;

import java.util.List;



import entity.Categorie;
import exceptions.CategorieNotFoundException;

public interface CategorieService {
		
	public void update(Categorie categorie)throws CategorieNotFoundException;
	
	public Categorie getCategorieById(long idCategorie) throws CategorieNotFoundException;
	
	public List <Categorie> getAllCategories();

	public void deleteCategorie(long idCategorie)throws CategorieNotFoundException; 
	
	public void create(Categorie newCat);
}
