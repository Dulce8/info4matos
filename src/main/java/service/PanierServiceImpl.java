package service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.PanierDao;
import entity.Article;
import entity.Panier;
import exceptions.ArticleNonDisponibleException;
import exceptions.PanierNotFoundException;

@Service
public class PanierServiceImpl implements PanierService {

	private PanierDao panierDao;
	
	public PanierServiceImpl(PanierDao panierDao) {
		this.panierDao = panierDao;
	}

	@Override
	public void update(Panier panier) {
		// TODO Auto-generated method stub

	}

	@Override
	public void create(Panier panier) {
		panierDao.createPanier(panier);

	}

	@Override
	public List<Panier> getAllPaniers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Panier getPanierById(long id) throws PanierNotFoundException {
		Panier panier = panierDao.get(id);
		if(panier == null) {
			throw new PanierNotFoundException();
		}
		return panier;
	}

	@Override
	@Transactional(rollbackFor = ArticleNonDisponibleException.class)
	public Panier validatePanier(long id) throws ArticleNonDisponibleException, PanierNotFoundException {
		Panier panier = getPanierById(id);
		List<Article> listArticles= panier.getArticles();
		for(Article article : listArticles) {
			long stock = article.getStock();
			if(stock <= 0) {
				throw new ArticleNonDisponibleException();
			}
			article.setStock(--stock);
		}
		return panier;
	}

	@Override
	public void delete(long id) throws PanierNotFoundException {
		getPanierById(id);
		panierDao.deletePanier(id);
		
	}

}
