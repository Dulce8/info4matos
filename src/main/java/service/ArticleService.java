package service;

import java.util.List;

import entity.Article;
import exceptions.ArticleNotFoundException;

public interface ArticleService {
	public void update(Article article) throws ArticleNotFoundException;
	public Article getArticleById(long idArticle) throws ArticleNotFoundException;
	public List<Article> getAllArticles();
	public void deleteArticle(long idArticle) throws ArticleNotFoundException;
	public void create (Article article);
}
